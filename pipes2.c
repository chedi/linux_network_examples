#include <stdio.h> 
#include <unistd.h>
#include <assert.h>
#include <sys/wait.h>
#include <sys/types.h>

int main (int argc, char** argv){
	pid_t pid;
	int   result;
	int   file_descriptors[2];
	
	assert(argc == 3);

	result = pipe(file_descriptors);
	if(result < 0){
		perror("Error creating the pipe");
		return 1;
	}

	pid = fork();
	if(pid < 0){
		perror("Error creating child process");
		return 2;
	}

	if(pid == 0) {
		close (file_descriptors[1]);
		dup2 (file_descriptors[0], STDIN_FILENO);
		execlp (argv[2], argv[2], 0);
	} else {
		FILE* stream;
		close (file_descriptors[0]);
		dup2 (file_descriptors[1], STDOUT_FILENO);
		execlp (argv[1], argv[1], 0);
		close (file_descriptors[1]);
		waitpid (pid, NULL, 0);
	}
	return 0;
}
