#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

const int MAX_MESSAGE_LENGTH = 256;

void writer (const char* message, FILE* stream){
	fprintf (stream, "Recieved message : %s\n", message);
	fflush  (stream);
}

void reader (FILE* stream){
	char buffer[1024];
	while (!feof (stream) && !ferror (stream) && fgets (buffer, sizeof (buffer), stream) != NULL){
		fputs (buffer, stdout);	
	}
}


int main(int argc, char** argv){
	pid_t pid;
	int   result;
	int   file_descriptors[2];

	result = pipe(file_descriptors);
	if(result < 0){
		perror("Error creating the pipe");
		return 1;
	}

	pid = fork();
	if(pid < 0){
		perror("Error creating child process");
		return 2;
	}

	if(pid == 0){
		//Child process
		FILE* stream;
		close (file_descriptors[1]);
		stream = fdopen (file_descriptors[0], "r");
		reader (stream);
		close(file_descriptors[0]);
	} else {
		//Parent prcess
		FILE* stream;
		char message[MAX_MESSAGE_LENGTH];
		close (file_descriptors[0]);
		stream = fdopen (file_descriptors[1], "w");
		scanf("%s", message);
		writer (message, stream);
		close(file_descriptors[1]);
		waitpid (pid, NULL, 0);
	}
	return 0;
}